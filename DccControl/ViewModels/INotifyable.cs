﻿using System.ComponentModel;

namespace DccControl.ViewModels
{
    public interface INotifyable : INotifyPropertyChanged
    {
    }
}
