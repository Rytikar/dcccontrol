﻿using DccControl.ViewModels;
using System;
using System.Windows;

namespace DccControl.Services
{
    public interface IViewFactory
    {
        void Register<TViewModel, TView>() where TViewModel : class, IViewModel where TView : Window;
        Window Resolve<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : class, IViewModel;
    }
}
