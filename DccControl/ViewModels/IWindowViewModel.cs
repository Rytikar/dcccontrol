﻿namespace DccControl.ViewModels
{
    public interface IWindowViewModel : IViewModel
    {
        string Title { get; set; }
    }
}
