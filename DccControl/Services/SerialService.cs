﻿using RJCP.IO.Ports;
using System;
using System.Linq;
using System.Management;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DccControl.Services
{
    public class SerialService : ISerialService
    {
        public const char ArgumentDelimiter = ',';
        public const char ConnectRequest = 'C';
        public const char VoltsRequest = 'V';
        public const char OutputStateRequest = 'O';
        public const char MemoryRequest = 'M';
        public const char StoreRequest = 'S';
        public const char TransientRequest = 'T';
        public const char FirmwareRequest = 'F';
        public const char StatusResponse = 'S';
        public const char MemoryResponse = 'M';
        public const char FirmwareResponse = 'F';

        private readonly SerialPortStream _serialPortStream;
        private readonly CultureInfo _culture;

        public event DccResponseDelegate DccResponseReceived;
        public event DccErrorDelegate DccErrorReceived;

        public SerialService(SerialPortStream serialPortStream, CultureInfo culture)
        {
            _serialPortStream = serialPortStream ?? throw new ArgumentNullException(nameof(serialPortStream));
            _culture = culture ?? throw new ArgumentNullException(nameof(culture));
            _serialPortStream.DataReceived += SerialPortStreamDataReceived;
            _serialPortStream.ErrorReceived += SerialPortStreamErrorReceived;
        }

        private void SerialPortStreamErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            DccErrorReceived?.Invoke(e.EventType.ToString());
        }

        private void SerialPortStreamDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var response = _serialPortStream.ReadLine();
            if (response == null || response.Length < 2) return;
            
            byte checkSum = (byte)response[response.Length-1];
            response = response.Substring(0, response.Length - 1);
            byte ccs = CalcCheckSum(response);
            if (checkSum != CalcCheckSum(response)) return;

            CallResponseDelegate(response);
        }

        private void CallResponseDelegate(string response)
        {
            if (string.IsNullOrEmpty(response)) throw new ArgumentNullException(nameof(response));
            switch(response[0])
            {
                case StatusResponse: RaiseDccResponseReceived(new DccStatusResponse(_culture, response.Substring(1))); break;
                case MemoryResponse: RaiseDccResponseReceived(new DccMemoryResponse(_culture, response.Substring(1))); break;
                case FirmwareResponse: RaiseDccResponseReceived(new DccFirmwareResponse(_culture, response.Substring(1))); break;
            }
        }

        private void RaiseDccResponseReceived(DccResponse response)
        {
            DccResponseReceived?.Invoke(response);
        }

        private byte CalcCheckSum(string s)
        {
            int checkSum = 0;
            foreach (var c in s) checkSum += (byte)c;
            checkSum = checkSum % 256;
            if (checkSum > 128) checkSum -= 128;
            if (checkSum < 32) checkSum += 32;
            return (byte)checkSum;
        }

        public bool IsConnected => _serialPortStream.IsOpen;

        public string Port => IsConnected ? _serialPortStream.PortName : string.Empty;

        public int BaudRate => IsConnected ? _serialPortStream.BaudRate : 0;

        public void Close()
        {
            if (!IsConnected) return;
            _serialPortStream.Close();
        }

        public IList<SerialInformation> GetSerialInformation()
        {
            var result = new List<SerialInformation>();
            try
            {
                using (var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity"))
                {
                    var portNames = SerialPortStream.GetPortNames();
                    foreach (var o in searcher.Get())
                    {
                        var queryObj = (ManagementObject)o;
                        var caption = queryObj["Caption"];
                        if (caption == null) continue;
                        result.AddRange(from portName in portNames where caption.ToString().Contains($"({portName})") select new SerialInformation(portName, caption.ToString()));
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                DccErrorReceived?.Invoke(exception.Message);
                return null;
            }
        }

        public ConnectInformation Open(string port, int baudRate)
        {
            try
            {
                _serialPortStream.PortName = port;
                _serialPortStream.BaudRate = baudRate;
                _serialPortStream.Encoding = Encoding.UTF8;
                _serialPortStream.ReadTimeout = 1000;
                _serialPortStream.WriteTimeout = 1000;

                _serialPortStream.Open();
                return new ConnectInformation(Port, BaudRate);
            }
            catch (Exception exception)
            {
                DccErrorReceived?.Invoke(exception.Message);
                return null;
            }
        }

        public void Write(char request, string arg = null)
        {
            var checkSum = WriteByte((byte)request);
            if (!string.IsNullOrEmpty(arg)) foreach (var c in arg) checkSum = WriteByte((byte)c, checkSum);
            if (checkSum > 128) checkSum -= 128;
            if (checkSum < 32) checkSum += 32;
            _serialPortStream.WriteByte(checkSum);
            _serialPortStream.WriteByte(10);
        }

        public void WriteArgs(char request, params string[] args)
        {
            var sb = new StringBuilder();
            for(int i = 0; i < args.Length; i++)
            {
                sb.Append(args[i]);
                if (i < args.Length-1) sb.Append(ArgumentDelimiter);
            }
            Write(request, sb.ToString());
        }

        private byte WriteByte(byte b, int checkSum = 0)
        {
            _serialPortStream.WriteByte(b);
            return (byte)((checkSum + b) % 256);
        }
    }
}
