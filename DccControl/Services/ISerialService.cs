﻿using System.Collections.Generic;

namespace DccControl.Services
{
    public delegate void DccResponseDelegate(DccResponse response);
    public delegate void DccErrorDelegate(string error);

    public interface ISerialService
    {
        ConnectInformation Open(string port, int baudRate);
        void Close();
        IList<SerialInformation> GetSerialInformation();
        bool IsConnected { get; }
        string Port { get; }
        int BaudRate { get; }
        void Write(char request, string arg = null);
        void WriteArgs(char request, params string[] args);
        event DccResponseDelegate DccResponseReceived;
        event DccErrorDelegate DccErrorReceived;
    }
}
