﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace DccControl.Services
{
    public class DccMemoryResponse : DccResponse
    {
        public const int MaxUserValues = 3;

        public DccMemoryResponse(CultureInfo culture, string arguments) : base(culture)
        {
            var split = arguments.Split(SerialService.ArgumentDelimiter);
            for (int i = 0; i < MaxUserValues; i++)
            {
                if (double.TryParse(split[i], NumberStyle, Culture, out double dValue)) UserFloatValues.Add(dValue);
                else throw new InvalidOperationException($"User float value is invalid in memory response, is '{split[i]}'");
                if (int.TryParse(split[i + MaxUserValues], NumberStyle, Culture, out int iValue)) UserIntValues.Add(iValue);
                else throw new InvalidOperationException($"User intt value is invalid in memory response, is '{split[i + MaxUserValues]}'");
            }
        }
        public IList<double> UserFloatValues { get; } = new List<double>();
        public IList<int> UserIntValues { get; } = new List<int>();
    }
}
