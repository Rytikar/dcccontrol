﻿using System;
using System.Windows.Threading;
using System.Windows;
using System.Globalization;
using Autofac;
using DccControl.ViewModels;
using DccControl.Services;
using RJCP.IO.Ports;

namespace DccControl
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
            DispatcherUnhandledException += AppDispatcherUnhandledException;
            var container = RegisterServices(new ContainerBuilder()).Build();

            //var configurationService = container.Resolve<IConfigService>();
            //CultureManager.UICulture = new System.Globalization.CultureInfo(configurationService.Configuration.SelectedCulture);

            var viewFactory = RegisterViews(container.Resolve<IViewFactory>(new NamedParameter("container", container)));
            MainWindow = viewFactory.Resolve<IMainWindowViewModel>();
            MainWindow.Show();
        }

        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            //if (exception != null) LogTools.Error("Unhandled exception in application", exception);
            //else LogTools.Error("Unhandled error in application");
        }

        private void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            //LogTools.Error("Unhandled exception in application", e.Exception);
            e.Handled = true;
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
        }

        private static ContainerBuilder RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterInstance(CultureInfo.CreateSpecificCulture("en-US"));
            builder.RegisterType<SerialPortStream>();
            builder.RegisterType<SerialService>().As<ISerialService>();
            
            builder.RegisterType<ViewModelFactory>().As<IViewModelFactory>();
            builder.RegisterType<ViewFactory>().As<IViewFactory>();
            builder.RegisterType<DateTimeService>().As<IDateTimeService>();
            builder.RegisterType<MainWindowViewModel>().As<IMainWindowViewModel>();
            builder.RegisterType<MainWindow>();

            return builder;
        }

        private static IViewFactory RegisterViews(IViewFactory viewFactory)
        {
            viewFactory.Register<IMainWindowViewModel, MainWindow>();

            return viewFactory;
        }
    }
}
