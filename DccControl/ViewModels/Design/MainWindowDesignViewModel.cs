﻿using DccControl.Services;
using DccControl.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DccControl.ViewModels.Design
{
    public class MainWindowDesignViewModel : WindowViewModel, IMainWindowViewModel
    {
        public void Closing() { throw new NotImplementedException(); }
        public bool IsConnected { get; }
        public IList<SerialInformation> AvailableSerialPorts => new List<SerialInformation> { new SerialInformation("COM7", "Serial Port")};
        public SerialInformation SelectedSerialPort { get; set; }
        public IRelayCommand ConnectCommand => throw new NotImplementedException();
        public string ConnectDisplayText => "Connect";

        public IRelayCommand OutputStateSetCommand => throw new NotImplementedException();
        public string SetOutputStateDisplayText => "ON";

        public double DccVolts => 0.0;
        public bool DccOutputState => false;
        public TransientMode DccTransientMode => TransientMode.Off;
        public string DccFirmwareVersion => "1.2";

        //---

        public IRelayCommand ConstantModeSetCommand => throw new NotImplementedException();
        public string ConstantModeGroupText => "Constant Mode";
        public string ConstantModeVoltsLabelText => "Volts";
        public string ConstantModeSetButtonText => "Set";
        public string ConstantModeVoltsInput { get => "11.999"; set => throw new NotImplementedException(); }

        //---

        public IRelayCommand TransientModeSetCommand => throw new NotImplementedException();
        public string TransientModeGroupText => "Transient Mode";
        public string TransientModeModeLabelText => "Mode";
        public IEnumerable<TransientMode> TransientModeModes => new List<TransientMode> { TransientMode.Toggle, TransientMode.Loop, TransientMode.Raise, TransientMode.Fall };
        public TransientMode TransientModeSelectedMode { get => TransientMode.Toggle; set => throw new NotImplementedException(); }
        public string TransientModeLoLabelText => "Lo";
        public string TransientModeLoInput { get => "1.999"; set => throw new NotImplementedException(); }
        public string TransientModeHiLabelText => "Hi";
        public string TransientModeHiInput { get => "11.999"; set => throw new NotImplementedException(); }
        public string TransientModeStepLabelText => "Step";
        public string TransientModeStepInput { get => "0.125"; set => throw new NotImplementedException(); }
        public string TransientModeDurationLabelText => "Duration mS";
        public string TransientModeDurationInput { get => "1500"; set => throw new NotImplementedException(); }
        public bool TransientModeStepEnabled { get => true; set => throw new NotImplementedException(); }
        public string TransientModeSetButtonText => "Set";

        //---

        public IRelayCommand UserValueSetCommand => throw new NotImplementedException();
        public IRelayCommand UserValueStoreCommand => throw new NotImplementedException();
        public string UserValueGroupText => "User Values";
        public string[] UserValueLabelText { get; } = { "Float 1", "Float 2", "Float 3", "Int 1", "Int 2", "Int 3" };
        public string UserFloat1Input { get => "0,1"; set => throw new NotImplementedException(); }
        public string UserFloat2Input { get => "1,5"; set => throw new NotImplementedException(); }
        public string UserFloat3Input { get => "10,9"; set => throw new NotImplementedException(); }
        public string UserInt1Input { get => "1"; set => throw new NotImplementedException(); }
        public string UserInt2Input { get => "10"; set => throw new NotImplementedException(); }
        public string UserInt3Input { get => "100"; set => throw new NotImplementedException(); }
        public string UserValueStoreText => "Store";
        public string UserValueSetText => "Set";
    }
}
