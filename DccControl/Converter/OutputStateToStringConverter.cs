﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DccControl.Converter
{
    public class OutputStateToStringConverter : IValueConverter
    {
        public const string OutputStateOnText = "ON";
        public const string OutputStateOffText = "OFF";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? OutputStateOnText : OutputStateOffText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
