﻿using System;

namespace DccControl.ViewModels
{
    public class PropertyCanChangeEventArgs : EventArgs
    {
        public string PropertyName { get; }
        public PropertyCanChangeEventArgs(string propertyName)
        {
            PropertyName = propertyName;
        }
    }

    public class PropertyCanChangeEventArgs<T> : PropertyCanChangeEventArgs
    {
        public T NewValue { get; }

        public PropertyCanChangeEventArgs(string propertyName, T newValue) : base(propertyName)
        {
            NewValue = newValue;
        }
    }
}
