﻿using System;
using System.Globalization;

namespace DccControl.Services
{
    public enum TransientMode { Off, Toggle, Loop, Raise, Fall }

    public class DccStatusResponse : DccResponse
    {
        public DccStatusResponse(CultureInfo culture, string arguments) : base(culture)
        {
            var split = arguments.Split(SerialService.ArgumentDelimiter);
            if (split.Length != 3) throw new InvalidOperationException("Need three arguments from status response");

            if (float.TryParse(split[0], NumberStyle, Culture, out float volts)) Volts = volts;
            else throw new InvalidOperationException($"Volts are invalid in status response, are '{split[0]}'");

            if (Enum.TryParse<TransientMode>(split[1], out TransientMode transientMode)) TransientMode = transientMode;
            else throw new InvalidOperationException($"Transient mode is invalid in status response, is '{split[1]}'");

            if (split[2] == "0" || split[2] == "1") IsOn = split[2] == "1";
            else throw new InvalidOperationException($"IsOn is invalid in status response, is '{split[2]}'");
        }
        public bool IsOn { get; private set; }
        public TransientMode TransientMode { get; private set; }
        public float Volts { get; private set; }
    }
}
