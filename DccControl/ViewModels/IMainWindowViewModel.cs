﻿using DccControl.Services;
using DccControl.Tools;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DccControl.ViewModels
{
    public interface IMainWindowViewModel : IViewModel
    {
        void Closing();
        bool IsConnected { get; }
        IList<SerialInformation> AvailableSerialPorts {get;}
        SerialInformation SelectedSerialPort { get; set; }
        IRelayCommand ConnectCommand { get; }
        IRelayCommand OutputStateSetCommand { get; }
        string ConnectDisplayText { get; }
        string SetOutputStateDisplayText { get; }
        double DccVolts { get; }
        bool DccOutputState { get; }
        TransientMode DccTransientMode { get; }
        string DccFirmwareVersion { get; }

        IRelayCommand ConstantModeSetCommand { get; }
        string ConstantModeGroupText { get; }
        string ConstantModeVoltsLabelText { get; }
        string ConstantModeSetButtonText { get; }
        string ConstantModeVoltsInput { get; set; }

        IRelayCommand TransientModeSetCommand { get; }
        string TransientModeGroupText { get; }
        string TransientModeModeLabelText { get; }
        IEnumerable<TransientMode> TransientModeModes { get; }
        TransientMode TransientModeSelectedMode { get; set; }
        string TransientModeLoLabelText { get; }
        string TransientModeLoInput { get; set; }
        string TransientModeHiLabelText { get; }
        string TransientModeHiInput { get; set; }
        string TransientModeStepLabelText { get; }
        string TransientModeStepInput { get; set; }
        bool TransientModeStepEnabled { get; set; }
        string TransientModeDurationLabelText { get; }
        string TransientModeDurationInput { get; set; }
        string TransientModeSetButtonText { get; }

        string UserValueGroupText { get; }
        IRelayCommand UserValueSetCommand { get; }
        IRelayCommand UserValueStoreCommand { get; }
        string[] UserValueLabelText { get; }
        string UserFloat1Input { get; set; }
        string UserFloat2Input { get; set; }
        string UserFloat3Input { get; set; }
        string UserInt1Input { get; set; }
        string UserInt2Input { get; set; }
        string UserInt3Input { get; set; }
        string UserValueStoreText { get; }
        string UserValueSetText { get; }
    }
}
