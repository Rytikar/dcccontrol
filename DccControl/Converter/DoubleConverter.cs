﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DccControl.Converter
{
    public class DoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return double.TryParse((string)value, out double d) ? d : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value).ToString("0.000");
        }
    }
}
