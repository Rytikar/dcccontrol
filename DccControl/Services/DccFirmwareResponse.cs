﻿using System.Globalization;

namespace DccControl.Services
{
    public class DccFirmwareResponse : DccResponse
    {
        public DccFirmwareResponse(CultureInfo culture, string arguments) : base(culture)
        {
            Version = arguments;
        }
        public string Version { get; private set; }
    }
}
