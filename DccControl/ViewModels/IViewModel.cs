﻿using System.Threading;

namespace DccControl.ViewModels
{
    public interface IViewModel : INotifyableBase
    {
        bool IsEnabled { get; set;}
        SynchronizationContext SynchronizationContext { get; set; }
    }
}
