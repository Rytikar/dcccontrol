﻿using System.Globalization;

namespace DccControl.Services
{
    public abstract class DccResponse
    {
        protected NumberStyles NumberStyle => NumberStyles.Number;
        protected CultureInfo Culture { get; private set; }

        protected DccResponse(CultureInfo culture)
        {
            Culture = culture;
        }
    }
}
