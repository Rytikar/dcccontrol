﻿namespace DccControl.Services
{
    public class ConnectInformation
    {
        public string Port { get; }
        public int BaudRate { get; }

        public ConnectInformation(string port, int baudRate)
        {
            Port = port;
            BaudRate = baudRate;
        }
    }
}
