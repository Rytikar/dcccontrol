﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace DccControl.Converter
{
    public class OutputStateToBrushConverter : IValueConverter
    {
        static public Brush OutputStateOnBrush = new SolidColorBrush(Colors.Green);
        static public Brush OutputStateOffBrush = new SolidColorBrush(Colors.Blue);
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? OutputStateOnBrush : OutputStateOffBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
