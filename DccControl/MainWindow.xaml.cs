﻿using DccControl.ViewModels;
using System.Windows;

namespace DccControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!(DataContext is IMainWindowViewModel vm)) return;
            vm.Closing();
        }
    }
}
