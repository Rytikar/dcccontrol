#Introduction#

The DccControl is a Windows software to remote control the [Scullcom DC Calibrator](https://www.youtube.com/watch?v=V9l5kc_o_dA&list=PLUMG8JNssPPzKeYkrDJpXV0J0Eu5myQqi)
You will need to use the [dccalibrator](https://bitbucket.org/Rytikar/dccalibrator/src/master/) Arduino firmware I've implemented to use DccControl.
Also, as the firmware uses the fast TFT library, the hardware changes to the TFT print with the 1.2k/1.8k voltage divider modification is needed.

### Release Versions ###
* 1.0, 13.Aug.2018, first release
* 1.1, 16.Oct.2018, added functionality for transient mode in dccalibrator firmware 1.3. Ui reworked.

Please report errors if you find some!

This readme and documentation is under construction...

### Installation ###

* Click on the [Downloads](https://bitbucket.org/Rytikar/dcccontrol/downloads/) link.
* Download the 'Setup.msi'.
* Run the installer.
* Run the software. No special icon is added as of now and .Net framework 7.1 is needed.
* Have fun.

### Documentation ###

DccControl is actually a study in what it takes to remote control an Arduino, so it does not add fancy features to the DC Voltage Calibrator (as of now).
I will use the knowledge gained to attempt to remote control the Scullcom DC Load in the future.

You can set the voltage by user input or use one of the three user stored values available.
Control transient mode. 
You can also store user defined values for volts and duration on the Arduino of the DC Voltage Calibrator.

When the device is connected to the computer via the usb cable, a serial port should be detected by Windows.
Select the appropriate port with the dropdown at the bottom of the DccControl screen.
After pressing the 'Connect' button, the DC Voltage Calibrator will reset and then show a 'remote' label on the TFT.
It is now controlled by DccControl and manual inputs are disabled.
After pressing 'Disconnect' the unit will go back to manual control.
Due to the update frequency beeing once a second, fast transients can not be traced precisely on the remote software.
The DC Calibrator will work and output normally.

The rest of the UI should be simple to use. If there are more questions, contact me.

### Who do I talk to? ###

* dps@rosenkilde-hoppe.dk

### Thanks ###

To:

* [Scullcom](http://www.scullcom.uk/)
