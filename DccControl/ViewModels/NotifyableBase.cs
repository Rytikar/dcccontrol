﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace DccControl.ViewModels
{
    public delegate void PropertyCanChangeEventHandler(object sender, PropertyCanChangeEventArgs e);

    public interface IPropertyCanChange
    {
        event PropertyCanChangeEventHandler PropertyCanChange;
    }

    public interface INotifyableBase : INotifyable, IPropertyCanChange
    { }

    public abstract class NotifyableBase : INotifyableBase
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyCanChangeEventHandler PropertyCanChange;

        protected virtual bool SetProperty<TProperty>(ref TProperty property, TProperty value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(property, value)) return false;

            property = value;
            OnPropertyChanged(propertyName);

            return true;
        }

        protected void OnPropertyCanChange<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            PropertyCanChange?.Invoke(this, new PropertyCanChangeEventArgs<T>(propertyName, newValue));
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void OnPropertyChanged<TProperty>(Expression<Func<TProperty>> propertyExpression)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            OnPropertyChanged(ExtractPropertyName(propertyExpression));
        }
        
        private static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null) throw new ArgumentNullException(nameof(propertyExpression));

            var memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null) throw new ArgumentException("The expression is not a member access expression.", nameof(propertyExpression));

            var property = memberExpression.Member as PropertyInfo;
            if (property == null) throw new ArgumentException("The member access expression does not access a property.", nameof(propertyExpression));

            var getMethod = property.GetMethod;
            if (getMethod.IsStatic) throw new ArgumentException("The referenced property is a static property.", nameof(propertyExpression));

            return memberExpression.Member.Name;
        }
    }
}
