﻿using DccControl.ViewModels;

namespace DccControl.Services
{
    public interface IViewModelFactory
    {
        T Resolve<T>() where T : class, IViewModel;
    }
}
