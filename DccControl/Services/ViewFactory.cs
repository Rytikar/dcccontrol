﻿using DccControl.ViewModels;
using Autofac;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace DccControl.Services
{

    public class ViewFactory : IViewFactory
    {
        private readonly IContainer _container;
        private readonly IDictionary<Type, Type> _map = new Dictionary<Type, Type>();

        public ViewFactory(IContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public void Register<TViewModel, TView>() where TViewModel : class, IViewModel where TView : Window
        {
            _map.Add(typeof(TViewModel), typeof(TView));
        }

        public Window Resolve<TViewModel>(Action<TViewModel> setStateAction) where TViewModel : class, IViewModel
        {
            var viewModel = _container.Resolve<TViewModel>();
            var view = _container.Resolve(_map[typeof(TViewModel)]) as Window;
            if (view == null) throw new InvalidOperationException($"Could not resolve view for viewmodel type {typeof(TViewModel)}");
            viewModel.SynchronizationContext = SynchronizationContext.Current;
            view.DataContext = viewModel;
            return view;
        }
    }
}
