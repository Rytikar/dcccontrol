﻿using System.Threading;

namespace DccControl.ViewModels
{
    public class ViewModel : NotifyableBase, IViewModel
    {
        private bool _isEnabled = true;

        public SynchronizationContext SynchronizationContext { get; set; }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }
    }
}
