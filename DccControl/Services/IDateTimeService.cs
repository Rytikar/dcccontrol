﻿using System;

namespace DccControl.Services
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}
