﻿using System.Windows.Input;

namespace DccControl.Tools
{
    public interface IRelayCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
