﻿using System;
using System.Linq;
using System.Windows;
using System.Reflection;
using DccControl.Tools;
using DccControl.Services;
using System.Collections.Generic;
using System.Threading;
using DccControl.Converter;
using System.Globalization;
using System.ComponentModel;

namespace DccControl.ViewModels
{
    public class MainWindowViewModel : WindowViewModel, IDataErrorInfo, IMainWindowViewModel
    {
        private readonly string[] CompatibleFirmware = { "1.3" };

        const double MinVolts = 0;
        const double MaxVolts = 12;
        const int MinDuration = 0;
        const int MaxDuration = 9999;
        const int MaxUserValues = 6;
        private readonly Timer _timer;
        private readonly ISerialService _serialService;
        private SerialInformation _selectedSerialPort;
        private ConnectInformation _connectInformation;
        private CultureInfo _culture;
        private long _statusReceived;

        private string _constantModeVoltsInput;
        private TransientMode _transientModeSelectedMode;
        private string _transientModeLoInput;
        private string _transientModeHiInput;
        private string _transientModeStepInput;
        private string _transientModeDurationInput;
        private bool _transientModeStepEnabled = true;
        private string _userFloat1Input;
        private string _userFloat2Input;
        private string _userFloat3Input;
        private string _userInt1Input;
        private string _userInt2Input;
        private string _userInt3Input;
        private string[] _userInputs = new string[MaxUserValues];

        private double _dccVolts;
        private bool _dccOutputState;
        private TransientMode _dccTransientMode;
        private string _dccFirmwareVersion;

        private string _error;
        private string _baseTitle;

        public MainWindowViewModel(ISerialService serialService, CultureInfo culture)
        {
            _serialService = serialService ?? throw new ArgumentNullException(nameof(serialService));
            _culture = culture ?? throw new ArgumentNullException(nameof(culture));
            _serialService.DccResponseReceived += SerialServiceDccResponseReceived;
            _serialService.DccErrorReceived += SerialServiceDccErrorReceived;

            _timer = new Timer(TimerElapsed);

            ConnectCommand = new RelayCommand(ConnectExecute, ConnectCanExecute);
            OutputStateSetCommand = new RelayCommand(SetOutputStateExecute, SetOutputStateCanExecute);
            ConstantModeSetCommand = new RelayCommand(ConstantModeSetExecute, ConstantModeSetCanExecute);
            TransientModeSetCommand = new RelayCommand(TransientModeSetExecute, TransientModeSetCanExecute);
            UserValueSetCommand = new RelayCommand(SetUserValueExecute, SetUserValueCanExecute);
            UserValueStoreCommand = new RelayCommand(StoreUserValueExecute, StoreUserValueCanExecute);

            _baseTitle = $"DccControl {VersionNumber.Major}.{VersionNumber.Minor}";
            Title = _baseTitle;

            ConstantModeVoltsInput = "0";
            TransientModeLoInput = "0";
            TransientModeHiInput = "0";
            TransientModeStepInput = "0";
            TransientModeDurationInput = "0";
            UserFloat1Input = "0";
            UserFloat2Input = "0";
            UserFloat3Input = "0";
            UserInt1Input = "0";
            UserInt2Input = "0";
            UserInt3Input = "0";
        }

        public void Closing()
        {
            Disconnect();
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == nameof(ConstantModeVoltsInput)) Error = VoltsError(ConstantModeVoltsInput);
                else if (columnName == nameof(TransientModeLoInput)) Error = VoltsError(TransientModeLoInput);
                else if (columnName == nameof(TransientModeHiInput)) Error = VoltsError(TransientModeHiInput);
                else if (columnName == nameof(TransientModeStepInput)) Error = VoltsError(TransientModeStepInput);
                else if (columnName == nameof(TransientModeDurationInput)) Error = DurationError(TransientModeDurationInput);
                else if (columnName == nameof(UserFloat1Input)) Error = VoltsError(UserFloat1Input);
                else if (columnName == nameof(UserFloat2Input)) Error = VoltsError(UserFloat2Input);
                else if (columnName == nameof(UserFloat3Input)) Error = VoltsError(UserFloat3Input);
                else if (columnName == nameof(UserInt1Input)) Error = DurationError(UserInt1Input);
                else if (columnName == nameof(UserInt2Input)) Error = DurationError(UserInt2Input);
                else if (columnName == nameof(UserInt3Input)) Error = DurationError(UserInt3Input);
                else Error = string.Empty;
                return Error;
            }
        }

        public string Error { get => _error; set => SetProperty(ref _error, value); }

        #region Properties

        private Version VersionNumber => Assembly.GetExecutingAssembly().GetName().Version;
        public IRelayCommand ConnectCommand { get; }
        public IRelayCommand OutputStateSetCommand { get; }

        public string ConnectDisplayText => IsConnected ? "Disconnect" : "Connect";
        public string SetOutputStateDisplayText => "Switch " + (DccOutputState ? OutputStateToStringConverter.OutputStateOffText : OutputStateToStringConverter.OutputStateOnText);
        public bool IsConnected => _serialService.IsConnected;
        public IList<SerialInformation> AvailableSerialPorts => _serialService.GetSerialInformation();
        public SerialInformation SelectedSerialPort { get => _selectedSerialPort; set { if (SetProperty(ref _selectedSerialPort, value)) UpdateConnectProperties(); } }

        public double DccVolts { get => _dccVolts; private set => SetProperty(ref _dccVolts, value); }
        public bool DccOutputState
        {
            get => _dccOutputState;
            private set
            {
                if (!SetProperty(ref _dccOutputState, value)) return;
                UpdateCommands();
                OnPropertyChanged(nameof(SetOutputStateDisplayText));
            }
        }
        public TransientMode DccTransientMode { get => _dccTransientMode; private set => SetProperty(ref _dccTransientMode, value); }
        public string DccFirmwareVersion { get => _dccFirmwareVersion; private set => SetProperty(ref _dccFirmwareVersion, value); }

        public IRelayCommand ConstantModeSetCommand { get; }
        public string ConstantModeGroupText => "Constant Mode";
        public string ConstantModeVoltsLabelText => "Volts";
        public string ConstantModeSetButtonText => "Set";
        public string ConstantModeVoltsInput { get => _constantModeVoltsInput; set { if (SetProperty(ref _constantModeVoltsInput, value)) UpdateCommands(); } }

        //---

        public IRelayCommand TransientModeSetCommand { get; }
        public string TransientModeGroupText => "Transient Mode";
        public string TransientModeModeLabelText => "Mode";
        public IEnumerable<TransientMode> TransientModeModes => new List<TransientMode> { TransientMode.Toggle, TransientMode.Loop, TransientMode.Raise, TransientMode.Fall };
        public TransientMode TransientModeSelectedMode { get => _transientModeSelectedMode; set { if (SetProperty(ref _transientModeSelectedMode, value)) TransientModeStepEnabled = _transientModeSelectedMode != TransientMode.Toggle; } }
        public string TransientModeLoLabelText => "Lo";
        public string TransientModeLoInput { get => _transientModeLoInput; set { if (SetProperty(ref _transientModeLoInput, value)) UpdateCommands(); } }
        public string TransientModeHiLabelText => "Hi";
        public string TransientModeHiInput { get => _transientModeHiInput; set { if (SetProperty(ref _transientModeHiInput, value)) UpdateCommands(); } }
        public string TransientModeStepLabelText => "Step";
        public string TransientModeStepInput { get => _transientModeStepInput; set { if (SetProperty(ref _transientModeStepInput, value)) UpdateCommands(); } }
        public string TransientModeDurationLabelText => "Duration mS";
        public string TransientModeDurationInput { get => _transientModeDurationInput; set { if (SetProperty(ref _transientModeDurationInput, value)) UpdateCommands(); } }
        public bool TransientModeStepEnabled { get => _transientModeStepEnabled; set => SetProperty(ref _transientModeStepEnabled, value); }
        public string TransientModeSetButtonText => "Set";

        //---

        public string UserValueGroupText => "User Values";
        public IRelayCommand UserValueSetCommand { get; }
        public IRelayCommand UserValueStoreCommand { get; }
        public string[] UserValueLabelText { get; } = { "Float 1", "Float 2", "Float 3", "Int 1", "Int 2", "Int 3" };
        public string UserFloat1Input { get => _userFloat1Input; set { if (SetProperty(ref _userFloat1Input, value)) SetUserInput(0, _userFloat1Input); } }
        public string UserFloat2Input { get => _userFloat2Input; set { if (SetProperty(ref _userFloat2Input, value)) SetUserInput(1, _userFloat2Input); } }
        public string UserFloat3Input { get => _userFloat3Input; set { if (SetProperty(ref _userFloat3Input, value)) SetUserInput(2, _userFloat3Input); } }
        public string UserInt1Input { get => _userInt1Input; set { if (SetProperty(ref _userInt1Input, value)) SetUserInput(3, _userInt1Input); } }
        public string UserInt2Input { get => _userInt2Input; set { if (SetProperty(ref _userInt2Input, value)) SetUserInput(4, _userInt2Input); } }
        public string UserInt3Input { get => _userInt3Input; set { if (SetProperty(ref _userInt3Input, value)) SetUserInput(5, _userInt3Input); } }
        public string UserValueStoreText { get => "Store"; }
        public string UserValueSetText { get => "Set"; }

        private bool ConnectionValid => _serialService.IsConnected;
        private long StatusReceived { get => _statusReceived; set { if (SetProperty(ref _statusReceived, value)) UpdateConnectProperties(); } }

        #endregion

        #region Eventhandlers

        private void SerialServiceDccResponseReceived(DccResponse response)
        {
            switch (response)
            {
                case DccStatusResponse _: HandleStatusResponse(response as DccStatusResponse); break;
                case DccMemoryResponse _: HandleMemoryResponse(response as DccMemoryResponse); break;
                case DccFirmwareResponse _: HandleFirmwareResponse(response as DccFirmwareResponse); break;
            }
        }

        private void SerialServiceDccErrorReceived(string error)
        {
            Error = error;
            Disconnect();
        }

        private void HandleStatusResponse(DccStatusResponse response)
        {
            DccVolts = response.Volts;
            DccTransientMode = response.TransientMode;
            DccOutputState = response.IsOn;

            Console.WriteLine($"DccVolts: {DccVolts}, DccTransientMode: {DccTransientMode}, DccOutputState: {DccOutputState}");

            if (StatusReceived == 0)
            {
                _serialService.Write(SerialService.FirmwareRequest);
                _serialService.Write(SerialService.MemoryRequest);
            }

            StatusReceived++;
        }

        private void HandleMemoryResponse(DccMemoryResponse response)
        {
            UserFloat1Input = response.UserFloatValues[0].ToString();
            UserFloat2Input = response.UserFloatValues[1].ToString();
            UserFloat3Input = response.UserFloatValues[2].ToString();

            UserInt1Input = response.UserIntValues[0].ToString();
            UserInt2Input = response.UserIntValues[1].ToString();
            UserInt3Input = response.UserIntValues[2].ToString();
        }

        private void HandleFirmwareResponse(DccFirmwareResponse response)
        {
            if (!CompatibleFirmware.Contains(response.Version))
            {
                Disconnect();
                MessageBox.Show($"The firmware version {response.Version} is not supported by this release!", "Compatibility Error");
                return;
            }
            
            DccFirmwareVersion = response.Version;
            Title = $"{_baseTitle} (Firmware {DccFirmwareVersion})";
        }

        #endregion

        #region Commands

        protected void ConnectExecute(object obj)
        {
            try
            {
                if (_serialService.IsConnected) Disconnect();
                else Connect(SelectedSerialPort);
                UpdateConnectProperties();
            }
            catch (Exception exception)
            {
                Error = exception.Message;
            }
        }

        protected bool ConnectCanExecute(object obj)
        {
            return SelectedSerialPort != null;
        }

        protected void SetOutputStateExecute(object obj)
        {
            _serialService.Write(SerialService.OutputStateRequest, DccOutputState ? "0" : "1");
        }

        protected bool SetOutputStateCanExecute(object obj)
        {
            return ConnectionValid;
        }

        protected void ConstantModeSetExecute(object obj)
        {
            _serialService.Write(SerialService.VoltsRequest, WriteableFloat(ConstantModeVoltsInput));
        }

        protected bool ConstantModeSetCanExecute(object obj)
        {
            return ConnectionAndInputValid(ConstantModeVoltsInput);
        }

        protected void TransientModeSetExecute(object obj)
        {
            _serialService.WriteArgs(
                SerialService.TransientRequest,
                WriteableFloat(TransientModeLoInput),
                WriteableFloat(TransientModeHiInput),
                WriteableFloat(TransientModeStepInput),
                WriteableInt(TransientModeDurationInput),
                ((int)TransientModeSelectedMode).ToString());
        }

        protected bool TransientModeSetCanExecute(object obj)
        {
            return ConnectionAndInputValid(TransientModeLoInput, TransientModeHiInput, TransientModeStepInput, TransientModeDurationInput);
        }

        protected void SetUserValueExecute(object obj)
        {
            var idx = Convert.ToInt32(obj);
            _serialService.Write(SerialService.VoltsRequest, WriteableFloat(_userInputs[idx]));
        }

        protected bool SetUserValueCanExecute(object obj)
        {
            var idx = Convert.ToInt32(obj);
            return ConnectionAndInputValid(_userInputs[idx]);
        }

        protected void StoreUserValueExecute(object obj)
        {
            var idx = Convert.ToInt32(obj);
            StoreUserValue(idx, WriteableFloat(_userInputs[idx]));
        }

        protected bool StoreUserValueCanExecute(object obj)
        {
            var idx = Convert.ToInt32(obj);
            return ConnectionAndInputValid(_userInputs[idx]);
        }

        #endregion

        #region private

        private void SetUserInput(int idx, string value)
        {
            _userInputs[idx] = value;
            UpdateCommands();
        }

        private void StoreUserValue(int slot, string value)
        {
            if (slot < 3) _serialService.WriteArgs(SerialService.StoreRequest, WriteableBool(false), WriteableInt(slot), value);
            else _serialService.WriteArgs(SerialService.StoreRequest, WriteableBool(true), WriteableInt(slot-3), value);
        }

        private string VoltsError(string value)
        {
            if (!double.TryParse(value, out double d)) return "Not a valid number!";
            if (d < MinVolts) return $"Value needs to be {MinVolts} or more!";
            if (d > MaxVolts) return $"Value needs to be {MaxVolts} or less!";
            return string.Empty;
        }

        private string DurationError(string value)
        {
            if (!int.TryParse(value, out int i)) return "Not a valid number!";
            if (i < MinDuration) return $"Value needs to be {MinDuration} or more!";
            if (i > MaxDuration) return $"Value needs to be {MaxDuration} or less!";
            return string.Empty;
        }

        private bool ConnectionAndInputValid(params string[] values)
        {
            return ConnectionValid && values.All(x => !string.IsNullOrEmpty(x));
        }

        private string WriteableFloat(string value)
        {
            return WriteableFloat(double.Parse(value));
        }

        private string WriteableInt(string value)
        {
            return WriteableInt(int.Parse(value));
        }

        private string WriteableFloat(double volts)
        {
            return volts.ToString("0.000", _culture);
        }

        private string WriteableInt(int duration)
        {
            return duration.ToString();
        }

        private string WriteableBool(bool b)
        {
            return b ? "1" : "0";
        }

        private void TimerElapsed(object state)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (!_serialService.IsConnected) return;
                    _serialService.Write(SerialService.ConnectRequest);
                }
                catch (Exception exception)
                {
                    Error = exception.Message;
                    Disconnect();
                }
            });
        }

        private void UpdateCommands()
        {
            ConnectCommand.RaiseCanExecuteChanged();
            ConstantModeSetCommand.RaiseCanExecuteChanged();
            OutputStateSetCommand.RaiseCanExecuteChanged();
            UserValueSetCommand.RaiseCanExecuteChanged();
            UserValueStoreCommand.RaiseCanExecuteChanged();
        }

        private void UpdateConnectProperties()
        {
            OnPropertyChanged(nameof(IsConnected));
            OnPropertyChanged(nameof(ConnectionValid));
            OnPropertyChanged(nameof(ConnectionAndInputValid));
            OnPropertyChanged(nameof(ConnectDisplayText));
            UpdateCommands();
        }

        private void Connect(SerialInformation serialInformation)
        {
            if (_serialService.IsConnected) return;

            StatusReceived = 0;
            _connectInformation = _serialService.Open(serialInformation.Port, 9600);
            _timer.Change(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(1));
            UpdateConnectProperties();
        }

        private void Disconnect()
        {
            if (!_serialService.IsConnected) return;

            _timer.Change(TimeSpan.FromMilliseconds(-1), TimeSpan.FromMilliseconds(-1));
            _serialService.Close();
            _connectInformation = null;
            UpdateConnectProperties();
        }

        private void StoreUserFloatValue(int slot)
        {
            //_serialService.Write(SerialService.StoreRequest, $"0{SerialService.ArgumentDelimiter}{slot}{SerialService.ArgumentDelimiter}{WriteableInputVolts}");
            //_serialService.Write(SerialService.MemoryRequest);
        }

        private void StoreUserIntValue(int slot)
        {
            //_serialService.Write(SerialService.StoreRequest, $"1{SerialService.ArgumentDelimiter}{slot}{SerialService.ArgumentDelimiter}{WriteableInputVolts}");
            //_serialService.Write(SerialService.MemoryRequest);
        }

        #endregion
    }
}
