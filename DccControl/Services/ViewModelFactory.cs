﻿using DccControl.ViewModels;
using Autofac;
using System;

namespace DccControl.Services
{

    public class ViewModelFactory : IViewModelFactory
    {
        private readonly IContainer _container;

        public ViewModelFactory(IContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public T Resolve<T>() where T : class, IViewModel
        {
            return _container.Resolve<T>();
        }
    }
}
